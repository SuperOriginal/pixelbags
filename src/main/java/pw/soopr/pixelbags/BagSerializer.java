package pw.soopr.pixelbags;

import com.google.gson.*;
import io.ibj.JLib.utils.Colors;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by Aaron.
 */
public class BagSerializer implements JsonSerializer<Bag>,JsonDeserializer<Bag>{
    @Override
    public Bag deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject object = jsonElement.getAsJsonObject();
        ItemStack icon;
        String id = object.get("template").getAsJsonObject().get("icon").getAsString();
        if(PixelBags.getI().getBagHandler().getTemplate(id) != null){
            icon = PixelBags.getI().getBagHandler().getTemplate(id).getIcon();
        }
        else
            icon = jsonDeserializationContext.deserialize(object.get("template").getAsJsonObject().get("backupicon"), ItemStack.class);
        BagTemplate template = new BagTemplate(Colors.colorify(object.get("template").getAsJsonObject().get("visualId").getAsString())
                    , object.get("template").getAsJsonObject().get("systemId").getAsString()
                    , BagTemplate.BagType.valueOf(object.get("template").getAsJsonObject().get("type").getAsString())
                    , object.get("template").getAsJsonObject().get("size").getAsInt()
                    , object.get("template").getAsJsonObject().get("max").getAsInt()
                    , icon);

        ItemStack[] items = new ItemStack[template.getSize()];
        JsonObject itemsObject = object.get("items").getAsJsonObject();
        for (Map.Entry<String, JsonElement> entry : itemsObject.entrySet()) {
            int i = Integer.parseInt(entry.getKey());
            ItemStack item = jsonDeserializationContext.deserialize(entry.getValue(), ItemStack.class);
            items[i] = item;
            //Bukkit.broadcastMessage("adding item to slot " + i + " with item " + item);
        }
        return new Bag(template,items,null,0,null);
    }

    @Override
    public JsonElement serialize(Bag bag, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject ret = new JsonObject();
            JsonObject template = new JsonObject();
            template.addProperty("type", bag.getTemplate().getType().toString());
            template.addProperty("max", bag.getTemplate().getMax());
            template.addProperty("size", bag.getTemplate().getSize());
            template.addProperty("systemId",bag.getTemplate().getSystemId());
            template.addProperty("visualId", Colors.decolorify(bag.getTemplate().getVisualId()));
            template.addProperty("icon",bag.getTemplate().getSystemId());
            template.add("backupicon", jsonSerializationContext.serialize(bag.getTemplate().getIcon(), ItemStack.class));
            ret.add("template", template);

            JsonObject items = new JsonObject();
            for (int i = 0; i < bag.getItems().length; i++) {
                ItemStack stack = bag.getItems()[i];
                if (stack != null) {
                    items.add(String.valueOf(i), jsonSerializationContext.serialize(stack, ItemStack.class));
                }
            }

            ret.add("items", items);
            return ret;
    }
}
