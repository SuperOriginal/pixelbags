package pw.soopr.pixelbags;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

/**
 * Created by Aaron.
 */
@Data
@AllArgsConstructor
public class BagViewingSession {
    private UUID currentViewer;
    private UUID beingViewed;
    private boolean main;

    public boolean getMain(){
        return main;
    }

    public boolean isSamePerson(){
        return currentViewer.toString().equals(beingViewed.toString());
    }
}
