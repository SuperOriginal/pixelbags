package pw.soopr.pixelbags;

import lombok.Value;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Aaron.
 */
@Value
public class BagTemplate {
    String visualId;
    String systemId;
    BagType type;
    int size;
    int max;
    ItemStack icon;

    public enum BagType{
        ITEM,
        MINER
    }
}
