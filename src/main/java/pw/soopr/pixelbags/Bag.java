package pw.soopr.pixelbags;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Aaron.
 */
@AllArgsConstructor
public class Bag implements Comparable<Bag>{
    @Getter
    BagTemplate template;
    @Getter
    ItemStack[] items;
    @Getter
    @Setter
    UUID owner;
    @Getter
    @Setter
    int slotId;
    @Getter
    @Setter
    private Inventory inv;

    public int compareTo(Bag compareFruit) {
        int compareQuantity = compareFruit.getTemplate().getSize();
        return compareQuantity - template.getSize();
    }

    public void populateInventory(){
        inv = Bukkit.createInventory(null,template.getSize(),template.getVisualId());
        for(int i = 0; i < items.length; i++){
            if(items[i] != null && items[i].getAmount() != 0) inv.setItem(i,items[i]);
        }
    }

    public void showToPlayer(Player p){
        p.openInventory(inv);
    }

    public void syncCachedItemsWithInventory(){
        for(int i = 0; i < template.getSize(); i++){
                //Bukkit.broadcastMessage("adding item to internals in slot " + i);
                items[i] = inv.getItem(i);
        }
    }

}
