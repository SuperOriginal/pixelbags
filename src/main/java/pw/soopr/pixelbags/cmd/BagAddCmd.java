package pw.soopr.pixelbags.cmd;

import io.ibj.JLib.PlayerLookup;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.soopr.pixelbags.BagHandler;
import pw.soopr.pixelbags.BagTemplate;
import pw.soopr.pixelbags.PixelBags;

import java.util.UUID;

/**
 * Created by Aaron.
 */
@Cmd(name="add",description = "Add a bag to a player's bags.",usage = "/bag add <player> <id>",perm = "pixel_bags.add",min = 2)
public class BagAddCmd implements ICmd{
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        final Player req = Bukkit.getPlayer(args.get(0).getAsString());
        final String reqId = args.get(1).getAsString();

        final BagHandler handler = PixelBags.getI().getBagHandler();
        if(!handler.isValidTemplate(reqId)){
            sender.sendMessage(ChatColor.RED + "A bag does not exist with that id.");
            return true;
        }

        BagTemplate template = handler.getTemplate(reqId);

        PixelBags.getI().runNow(new Runnable() {
            @Override
            public void run() {
                UUID playerUUID;
                if(req != null) playerUUID = req.getUniqueId();
                else{
                    playerUUID = new PlayerLookup().getFromName(args.get(0).getAsString());
                }

                if(playerUUID == null){
                    sender.sendMessage(ChatColor.RED + "The specified player does not exist.");
                    return;
                }

                int i = PixelBags.getI().getBagHandler().giveBagToPlayer(playerUUID,reqId);
                if(i == 0){
                    if(req != null) PixelBags.getI().getF("playerRecievesBag").replace("<id>",template.getVisualId()).sendTo(req);
                    sender.sendMessage("Gave " + args.get(0).getAsString() + " a PixelBag with id " + reqId);
                }else if(i == 1){
                    sender.sendMessage(ChatColor.RED + "Invalid bag id specified.");
                }else if(i == 3){
                    sender.sendMessage(ChatColor.RED + "The specified player does not exist.");
                }else{
                    sender.sendMessage(ChatColor.RED + "This user already has the maximum amount of bags.");
                }
            }
        }, ThreadLevel.ASYNC);

        return true;
    }
}
