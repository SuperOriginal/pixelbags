package pw.soopr.pixelbags.cmd;

import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.TreeCmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.soopr.pixelbags.PixelBags;

/**
 * Created by Aaron.
 */
@Cmd(name = "bag",
        description= "Opens your collection of bags.",
        usage = "/bag", aliases = {"bags"},
        perm = "pixel_bags.bag")
public class BagTreeCmd extends TreeCmd{
    public BagTreeCmd(){
        registerCmd(BagAddCmd.class);
        registerCmd(BagViewCmd.class);
    }

    @Override
    public boolean executeIfNoSubFound(CommandSender sender, ArgsSet args) throws PlayerException{
        if(!(sender instanceof Player)){
            help(sender);
            return true;
        }
        PixelBags.getI().getBagHandler().constructAndShowMenuToPlayer((Player) sender);
        return true;
    }
}
