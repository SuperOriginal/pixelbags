package pw.soopr.pixelbags.cmd;

import io.ibj.JLib.PlayerLookup;
import io.ibj.JLib.ThreadLevel;
import io.ibj.JLib.cmd.ArgsSet;
import io.ibj.JLib.cmd.ICmd;
import io.ibj.JLib.cmd.annotations.Cmd;
import io.ibj.JLib.exceptions.PlayerException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pw.soopr.pixelbags.PixelBags;

import java.util.UUID;

/**
 * Created by Aaron.
 */
@Cmd(name="view",description = "View a player's bags.",usage = "/bag view <player>",perm = "pixel_bags.view",min = 1)
public class BagViewCmd implements ICmd{
    @Override
    public boolean execute(CommandSender sender, ArgsSet args) throws PlayerException {
        if(!(sender instanceof Player)){
            return true;
        }

        Player reqPlayer = Bukkit.getPlayer(args.get(0).getAsString());
        if(reqPlayer != null){
            PixelBags.getI().getBagHandler().constructAndShowMenuToPlayer((Player)sender,reqPlayer.getUniqueId());
        }else{
            PixelBags.getI().runNow(new Runnable() {
                @Override
                public void run() {
                    UUID uuid = new PlayerLookup().getFromName(args.get(0).getAsString());
                    if(uuid == null || PixelBags.getI().getBagHandler().getBags(uuid) == null){
                        sender.sendMessage(ChatColor.RED + "The specified player does not exist.");
                    }else{
                        PixelBags.getI().getBagHandler().constructAndShowMenuToPlayer((Player)sender,uuid);
                    }
                }
            }, ThreadLevel.ASYNC);
        }
        return true;
    }
}
