package pw.soopr.pixelbags;

import com.google.gson.*;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.utils.Colors;
import io.ibj.JLib.utils.ItemMetaFactory;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;

/**
 * Created by Aaron.
 */
public class BagHandler implements ResourceReloadHook{

    @Getter
    Map<String,BagTemplate> templateSet;

    Map<UUID,List<Bag>> playerBags;

    @Getter
    List<BagViewingSession> mainMenuViewers;
    //Map<UUID,UUID> mainMenuViewers;

    @Getter
    Map<UUID, Bag> customBagViewers;

    Set<Material> validMinerBlocks;

    public BagHandler(){
        playerBags = new HashMap<>();
        mainMenuViewers = new ArrayList<>();
        customBagViewers = new HashMap<>();
        validMinerBlocks = new HashSet<>();
        initializeMinerSet();
    }

    private void registerTemplates(ConfigurationSection section){
        for(String key : section.getKeys(false)){

            List<String> lore = new ArrayList<>();
            section.getStringList(key+".lore").forEach(s -> lore.add(Colors.colorify(s)));

            //Bukkit.broadcastMessage("adding ler: " + lore);

            ItemStack icon = new ItemStack(Material.valueOf(section.getString(key+".material")));
            ItemMetaFactory.create(icon)
            .setDisplayName(Colors.colorify(section.getString(key + ".displayName")))
            .setLore(lore)
            .set();
            templateSet.put(key,new BagTemplate(icon.getItemMeta().getDisplayName() != null ? icon.getItemMeta().getDisplayName() : "Bag",key,BagTemplate.BagType.valueOf(section.getString(key+".type")),section.getInt(key+".size"),section.getInt(key+".max"),icon));
        }
    }

    @Override
    public void onReload(ResourceFile file) {
        FileConfiguration config = ((YAMLFile) file).getConfig();
        templateSet = new HashMap<>();
        registerTemplates(config.getConfigurationSection("bags"));
    }

    @SneakyThrows
    public void loadPlayer(UUID uuid){
        try(Connection connection = PixelBags.getI().getDBConnection()){
            PreparedStatement statement = connection.prepareStatement("select * from pixel_bags where uuid = ?");
            statement.setString(1,uuid.toString());

            ResultSet set = statement.executeQuery();

            insertPlayerToCache(uuid,decodeFromSet(connection,set,uuid));
        }
    }

    @SneakyThrows
    public List<Bag> decodeFromSet(Connection connection, ResultSet set, UUID uuid){
        if(!set.next()){
            PreparedStatement statement = connection.prepareStatement("insert into pixel_bags (uuid,bags) values (?,?)");
            statement.setString(1,uuid.toString());
            statement.setString(2,"");
            statement.execute();
            //Bukkit.broadcastMessage("Loading player with blank list");
            return new ArrayList<>();
        }else{
            String json = set.getString("bags");
            if(json.isEmpty() || json.equalsIgnoreCase("")){
                return new ArrayList<>();
            }
            //Bukkit.broadcastMessage(new JsonParser().parse(json).toString());
            JsonObject array = new JsonParser().parse(json).getAsJsonObject();
            JsonArray bags = array.getAsJsonArray("bags");

            ArrayList<Bag> list = new ArrayList<>();
            for(JsonElement el : bags){
                list.add(PixelBags.getI().getGson().fromJson(el.getAsJsonObject(),Bag.class));
            }

            Collections.sort(list);
            for(int i = 0; i < list.size(); i++){
                list.get(i).setSlotId(i);
                list.get(i).setOwner(uuid);
            }
            //Bukkit.broadcastMessage("Loading player with list");
            return list;
        }
    }

    private void insertPlayerToCache(UUID uuid, List<Bag> bags){
        playerBags.put(uuid,bags);
        for(Bag b : bags){
            b.populateInventory();
            b.setOwner(uuid);
        }
    }

    @SneakyThrows
    public void savePlayer(UUID uuid){
        try(Connection conn = PixelBags.getI().getDBConnection()){
            PreparedStatement statement = conn.prepareStatement("update pixel_bags set bags = ? where uuid = ?");
            JsonObject ret = new JsonObject();
            JsonArray array = new JsonArray();
            for(Bag b : playerBags.get(uuid)){
                array.add(PixelBags.getI().getGson().toJsonTree(b,Bag.class));
            }
            ret.add("bags",array);
            statement.setString(2,uuid.toString());
            //Bukkit.broadcastMessage("saving player: " + ret.toString());
            statement.setString(1,ret.toString());
            statement.executeUpdate();
            //Bukkit.broadcastMessage("updating db");
        }
        playerBags.remove(uuid);
    }

    public boolean isValidTemplate(String s){
        return templateSet.containsKey(s);
    }

    public BagTemplate getTemplate(String s){
        return templateSet.get(s);
    }

    @SneakyThrows
    public List<Bag> getBags(UUID uuid){
        if(playerBags.containsKey(uuid)) return playerBags.get(uuid);
        else{
            try (Connection connection = PixelBags.getI().getDBConnection()) {
                PreparedStatement stmt = connection.prepareStatement("select * from pixel_bags where uuid = ?");
                stmt.setString(1, uuid.toString());
                ResultSet set = stmt.executeQuery();
                if (!set.next()) {
                    return null;
                }
                set.beforeFirst();
                return decodeFromSet(connection, set, uuid);
            }
        }
    }

    public boolean cacheContainsPlayer(UUID uuid){
        return playerBags.containsKey(uuid);
    }

    /*
    0. Success
    1. Invalid Template
    2. Not Enough Room
    3. Player doesn't exist
     */
    @SneakyThrows
    public int giveBagToPlayer(UUID uuid, String id) {
        if (!isValidTemplate(id)) return 1;
        if (cacheContainsPlayer(uuid)) {
            int totalSameBag = 0;
            for (Bag b : playerBags.get(uuid)) {
                if (b.getTemplate().getSystemId().equalsIgnoreCase(id)) totalSameBag++;
            }
            if (totalSameBag >= getTemplate(id).getMax() || playerBags.get(uuid).size() + 1 > PixelBags.getI().getMainMenuSize())
                return 2;
            List<Bag> bags = getBags(uuid);
            bags.add(new Bag(getTemplate(id), new ItemStack[getTemplate(id).getSize()], uuid, bags.size(), null));
            Collections.sort(bags);
            for(int i = 0; i < bags.size(); i++){
                bags.get(i).setSlotId(i);
            }
            insertPlayerToCache(uuid, bags);
        } else {
            try (Connection connection = PixelBags.getI().getDBConnection()) {
                //Bukkit.broadcastMessage("Querying uuid: " + uuid);
                PreparedStatement stmt = connection.prepareStatement("select * from pixel_bags where uuid = ?");
                stmt.setString(1, uuid.toString());
                ResultSet set = stmt.executeQuery();
                if (!set.next()) {
                    //Bukkit.broadcastMessage("Entry not found for uuid");
                    return 3;
                }
                set.beforeFirst();
                //Bukkit.broadcastMessage("Entry found for uuid");
                List<Bag> bags = decodeFromSet(connection, set, uuid);
                int totalSameBag = 0;
                for (Bag b : bags) {
                    if (b.getTemplate().getSystemId().equalsIgnoreCase(id)) totalSameBag++;
                }
                if (totalSameBag >= getTemplate(id).getMax() || bags.size() + 1 > PixelBags.getI().getMainMenuSize()) return 2;
                bags.add(new Bag(getTemplate(id), new ItemStack[getTemplate(id).getSize()], uuid, bags.size(), null));
                Collections.sort(bags);
                for(int i = 0; i < bags.size(); i++){
                    bags.get(i).setSlotId(i);
                }
                insertPlayerToCache(uuid, bags);
                savePlayer(uuid);
            }
        }
        return 0;
    }

    public boolean isValidForType(BagTemplate.BagType type, Material mat){
        if(type == BagTemplate.BagType.MINER){
            return validMinerBlocks.contains(mat);
        }else if(type == BagTemplate.BagType.ITEM){
            return true;
        }
        return true;
    }

    public boolean isViewingMainMenu(UUID uuid){
        for(BagViewingSession session : mainMenuViewers){
            if(session.getCurrentViewer().toString().equals(uuid.toString())) return true;
        }
        return false;
    }

    public boolean isViewingCustomBag(UUID uuid){
        return customBagViewers.containsKey(uuid);
    }

    public void constructAndShowMenuToPlayer(Player p){
        Inventory inv = Bukkit.createInventory(p,PixelBags.getI().getMainMenuSize(),PixelBags.getI().getMainMenuName());
        for(Bag b : playerBags.get(p.getUniqueId())){
            inv.setItem(b.getSlotId(),b.getTemplate().getIcon());
        }
        p.openInventory(inv);
        mainMenuViewers.add(new BagViewingSession(p.getUniqueId(),p.getUniqueId(),true));
    }

    public void constructAndShowMenuToPlayer(Player p, UUID target){
        Inventory inv = Bukkit.createInventory(p,PixelBags.getI().getMainMenuSize(),PixelBags.getI().getMainMenuName());
        if(!cacheContainsPlayer(target)){
            loadPlayer(target);
        }
        for(Bag b : getBags(target)){
            inv.setItem(b.getSlotId(),b.getTemplate().getIcon());
        }
        p.openInventory(inv);
        mainMenuViewers.add(new BagViewingSession(p.getUniqueId(),target,true));
    }

    public void transferFromMainToBag(Player p, Bag bag){
        getSessionFromViewer(p.getUniqueId()).setMain(false);
        bag.showToPlayer(p);
        customBagViewers.put(p.getUniqueId(),bag);
    }

    public BagViewingSession getSessionFromViewer(UUID viewer){
        for(BagViewingSession session : mainMenuViewers){
            if(session.getCurrentViewer().toString().equals(viewer.toString())) return session;
        }
        return null;
    }

    public UUID getTargetUUID(UUID viewer){
        for(BagViewingSession session : mainMenuViewers){
            if(session.getCurrentViewer().toString().equals(viewer.toString())) return session.getBeingViewed();
        }
        return null;
    }

    public boolean isViewingSameMenu(UUID uuid){
        for(BagViewingSession session : mainMenuViewers){
            if(session.getCurrentViewer().toString().equals(uuid.toString()) && session.isSamePerson())return true;
        }
        return false;
    }

    public boolean nobodyElseIsViewing(UUID solo){
        UUID target = getTargetUUID(solo);
        //Bukkit.broadcastMessage(mainMenuViewers.toString());
        if(Bukkit.getPlayer(target) != null){
            //Bukkit.broadcastMessage("player is online. false.");
            return false;
        }
        for(BagViewingSession session : mainMenuViewers){
            if(!session.getCurrentViewer().toString().equals(solo.toString()) && session.getBeingViewed().toString().equals(target.toString())) return false;
        }
        //Bukkit.broadcastMessage("tru");
        return true;
    }

    public void unRegisterSession(UUID viewer){
        Iterator<BagViewingSession> iterator = mainMenuViewers.iterator();
        while(iterator.hasNext()){
            BagViewingSession session = iterator.next();
            if(session.getCurrentViewer() == viewer){
                iterator.remove();
                break;
            }
        }
    }

    private void initializeMinerSet(){
        for(Material mat : Material.values()){
            if(mat.toString().endsWith("_ORE")) validMinerBlocks.add(mat);
            if(mat.toString().endsWith("_INGOT")) validMinerBlocks.add(mat);
        }
        validMinerBlocks.add(Material.DIAMOND);
        validMinerBlocks.add(Material.COAL);
        validMinerBlocks.add(Material.REDSTONE);
        validMinerBlocks.add(Material.EMERALD);
        validMinerBlocks.add(Material.LAPIS_BLOCK);
        validMinerBlocks.add(Material.DIAMOND_BLOCK);
        validMinerBlocks.add(Material.COAL_BLOCK);
        validMinerBlocks.add(Material.REDSTONE_BLOCK);
        validMinerBlocks.add(Material.EMERALD_BLOCK);

    }
}
