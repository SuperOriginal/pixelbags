package pw.soopr.pixelbags.event;

import io.ibj.JLib.ThreadLevel;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import pw.soopr.pixelbags.Bag;
import pw.soopr.pixelbags.PixelBags;

/**
 * Created by Aaron.
 */
public class ClickEvent implements Listener{
    @EventHandler
    public void onClick(InventoryClickEvent e){
        if(PixelBags.getI().getBagHandler().isViewingMainMenu(e.getWhoClicked().getUniqueId()) && PixelBags.getI().getBagHandler().getSessionFromViewer(e.getWhoClicked().getUniqueId()).getMain()){
            e.setCancelled(true);
            for(Bag bag : PixelBags.getI().getBagHandler().getBags(PixelBags.getI().getBagHandler().getTargetUUID(e.getWhoClicked().getUniqueId()))){
                if(bag.getSlotId() == e.getRawSlot()){
                    PixelBags.getI().getBagHandler().transferFromMainToBag((Player)e.getWhoClicked(),bag);
                }
            }
        }else if(PixelBags.getI().getBagHandler().isViewingCustomBag(e.getWhoClicked().getUniqueId())){
            Bag b = PixelBags.getI().getBagHandler().getCustomBagViewers().get(e.getWhoClicked().getUniqueId());
            if(e.getAction().toString().startsWith("PLACE") && e.getRawSlot() >= 0 && e.getRawSlot() < b.getTemplate().getSize()){
                if(!PixelBags.getI().getBagHandler().isValidForType(b.getTemplate().getType(),e.getCursor().getType())) e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e){
        if(PixelBags.getI().getBagHandler().isViewingCustomBag(e.getPlayer().getUniqueId()) && !PixelBags.getI().getBagHandler().getSessionFromViewer(e.getPlayer().getUniqueId()).getMain()){
            //Bukkit.broadcastMessage("1");
            if(!PixelBags.getI().getBagHandler().getCustomBagViewers().get(e.getPlayer().getUniqueId()).getOwner().toString().equals(e.getPlayer().getUniqueId().toString()) && PixelBags.getI().getBagHandler().nobodyElseIsViewing(e.getPlayer().getUniqueId())){
                //Bukkit.broadcastMessage("Nobody else is viewing... saving");
                PixelBags.getI().runNow(new Runnable() {
                    @Override
                    public void run() {
                        PixelBags.getI().getBagHandler().getCustomBagViewers().get(e.getPlayer().getUniqueId()).syncCachedItemsWithInventory();
                        PixelBags.getI().getBagHandler().savePlayer(PixelBags.getI().getBagHandler().getTargetUUID(e.getPlayer().getUniqueId()));
                        //Bukkit.broadcastMessage("saved player: " + PixelBags.getI().getBagHandler().getTargetUUID(e.getPlayer().getUniqueId()));
                        PixelBags.getI().getBagHandler().getCustomBagViewers().remove(e.getPlayer().getUniqueId());
                        PixelBags.getI().getBagHandler().unRegisterSession(e.getPlayer().getUniqueId());
                    }
                }, ThreadLevel.ASYNC);
                return;
            }
            PixelBags.getI().getBagHandler().getCustomBagViewers().get(e.getPlayer().getUniqueId()).syncCachedItemsWithInventory();
            PixelBags.getI().getBagHandler().getCustomBagViewers().remove(e.getPlayer().getUniqueId());
            PixelBags.getI().getBagHandler().unRegisterSession(e.getPlayer().getUniqueId());
        }

        if(PixelBags.getI().getBagHandler().isViewingMainMenu(e.getPlayer().getUniqueId()) && PixelBags.getI().getBagHandler().getSessionFromViewer(e.getPlayer().getUniqueId()).getMain()){
           PixelBags.getI().getBagHandler().unRegisterSession(e.getPlayer().getUniqueId());
        }
    }
}
