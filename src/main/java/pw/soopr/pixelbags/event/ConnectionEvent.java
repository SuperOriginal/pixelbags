package pw.soopr.pixelbags.event;

import io.ibj.JLib.ThreadLevel;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import pw.soopr.pixelbags.PixelBags;

/**
 * Created by Aaron.
 */
public class ConnectionEvent implements Listener{

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        PixelBags.getI().runNow(new Runnable() {
            @Override
            public void run() {
                PixelBags.getI().getBagHandler().loadPlayer(e.getPlayer().getUniqueId());
            }
        }, ThreadLevel.ASYNC);
    }

    @EventHandler
    public void onKick(PlayerKickEvent e){
        PixelBags.getI().runNow(new Runnable() {
            @Override
            public void run() {
                PixelBags.getI().getBagHandler().savePlayer(e.getPlayer().getUniqueId());
            }
        }, ThreadLevel.ASYNC);
    }

    @EventHandler
    public void onLeave(PlayerQuitEvent e){
        PixelBags.getI().runNow(new Runnable() {
            @Override
            public void run() {
                PixelBags.getI().getBagHandler().savePlayer(e.getPlayer().getUniqueId());
            }
        }, ThreadLevel.ASYNC);
    }
}
