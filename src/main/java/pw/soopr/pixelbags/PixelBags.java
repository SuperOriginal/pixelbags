package pw.soopr.pixelbags;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.ibj.JLib.*;
import io.ibj.JLib.db.AuthenticatedDatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseConnectionDetails;
import io.ibj.JLib.db.DatabaseType;
import io.ibj.JLib.file.ResourceFile;
import io.ibj.JLib.file.ResourceReloadHook;
import io.ibj.JLib.file.YAMLFile;
import io.ibj.JLib.gson.ItemStackGsonSerializer;
import io.ibj.JLib.utils.Colors;
import lombok.Getter;
import lombok.SneakyThrows;
import org.bukkit.Bukkit;
import org.bukkit.block.Jukebox;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import pw.soopr.pixelbags.cmd.BagTreeCmd;
import pw.soopr.pixelbags.event.ClickEvent;
import pw.soopr.pixelbags.event.ConnectionEvent;

import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Created by Aaron.
 */
public class PixelBags extends JPlug{

    @Getter
    static PixelBags i;

    @Getter
    private BagHandler bagHandler;

    @Getter
    private Gson gson;

    private DatabaseConnectionDetails<Connection> databaseConnection;

    @Getter
    private String mainMenuName;

    @Getter
    private int mainMenuSize;

    private YAMLFile config;

    @Override
    public void onModuleEnable(){
        i = this;
        bagHandler = new BagHandler();
        config = new YAMLFile(this, "config.yml", bagHandler, new ResourceReloadHook() {
            @Override
            public void onReload(ResourceFile file) {
                FileConfiguration config = ((YAMLFile) file).getConfig();
                mainMenuName = Colors.colorify(config.getString("mainMenu.name"));
                mainMenuSize = config.getInt("mainMenu.size");
                if(mainMenuSize % 9 != 0){
                    throw new IllegalStateException("Menu size must be divisible by 9");
                }

                String ip = config.getString("host", "localhost");
                Integer port = config.getInt("port", 3306);
                String database = config.getString("database", "mc");
                if (config.contains("username")) {
                    String username = config.getString("username");
                    String password = config.getString("password");
                    //Bukkit.broadcastMessage(ip+port+database+username+password);
                    databaseConnection = new AuthenticatedDatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class, ip, port, database, username, password);
                } else {
                    databaseConnection = new DatabaseConnectionDetails<>(DatabaseType.SQL, Connection.class, ip, port, database);
                }
            }
        },bagHandler);

        gson = new GsonBuilder().registerTypeAdapter(Bag.class,new BagSerializer())
                .registerTypeAdapter(ItemStack.class,new ItemStackGsonSerializer())
                .create();

        registerEvents(new ClickEvent());
        registerEvents(new ConnectionEvent());
        registerCmd(BagTreeCmd.class);

        runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                try(Connection conn = getDBConnection()){
                    PreparedStatement stmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS pixel_bags (\n" +
                            "  uuid VARCHAR(64) NOT NULL,\n" +
                            "  bags LONGTEXT NOT NULL)");
                    stmt.execute();
                }
            }
        }, ThreadLevel.ASYNC);

        registerResource(config);
        config.reloadConfig();

        runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    bagHandler.loadPlayer(p.getUniqueId());
                }
            }
        }, ThreadLevel.ASYNC);
    }

    @Override
    public void onModuleDisable(){
        runNow(new Runnable() {
            @Override
            @SneakyThrows
            public void run() {
                for(Player p : Bukkit.getOnlinePlayers()){
                    bagHandler.savePlayer(p.getUniqueId());
                }
            }
        }, ThreadLevel.SYNC);
    }

    public Connection getDBConnection() {
        return JLib.getI().getDatabaseManager().getConnection(databaseConnection);
    }
}
